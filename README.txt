##How to use

#This library is using the Mailchimp API v.2.0

1. Load the library.
$this->load->library('Mailchimp');

2. Set the config details
$config['api_key'] = ''; //YOUR MAILCHIMP API KEY
$config['list-id'] = ''; //YOUR MAILING LIST ID

3. Load the config.
$this->config->load('mailchimp');

4. Use it.

//CHECK FOR USER/S IN YOUR LIST
$user = $MailChimp->call('lists/member-info', array(
    'id' => $this->config->item('list-id'),
    'emails' => array(
        array('email' => 'email@email.com')
      )
    )
);

var_dump($user);

5. For list of method you can use, look here : http://apidocs.mailchimp.com/api/2.0/#method-sections